<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Vehiculos;
use Auth;

class VehiculosController extends Controller{
    //
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(request $request){

        $userId = $request->user()->id;

        $vehiculos = Vehiculos::orderBy('id')->where('usuario', $userId)->paginate(10);

        return view('vehiculos.index', compact('vehiculos'));
    }

    // metodos ordenacion

    public function OrdMatriculaAsc()
    {
        $vehiculos = vehiculos::orderBy('matricula')->paginate(10);

        return view('vehiculos.index', compact('vehiculos'));
    }
    public function OrdMatriculaDesc()
    {
        $vehiculos = Vehiculos::orderBy('matricula', 'desc')->paginate(10);

        return view('vehiculos.index', compact('vehiculos'));
    }

    public function OrdMarcaAsc()
    {
        $vehiculos = Vehiculos::orderBy('marca')->paginate(10);

        return view('vehiculos.index', compact('vehiculos'));
    }
    public function OrdMarcaDesc()
    {
        $vehiculos = Vehiculos::orderBy('marca', 'desc')->paginate(10);

        return view('vehiculos.index', compact('vehiculos'));
    }
    public function OrdModeloAsc()
    {
        $vehiculos = Vehiculos::orderBy('modelo')->paginate(10);

        return view('vehiculos.index', compact('vehiculos'));
    }
    public function OrdModeloDesc()
    {
        $vehiculos = Vehiculos::orderBy('modelo', 'desc')->paginate(10);

        return view('vehiculos.index', compact('vehiculos'));
    }


    // Fin metodos de ordenacion

    public function show(){

        }

    public function store(Request $request){

        if ( !Auth::check() ) {
           return redirect('/')
                   ->with('info', 'Para dar de alta inicie sesión');
        }

        $reglas = [
            'matricula' => 'bail|required|unique:vehiculos|max:40',
            'marca' => 'required|max:40',
            'modelo' => 'required|max:40',
            'fotografia' => 'image|mimes:jpeg,png,jpg,gif|max:5000',
        ];

        $mensajes = [
            'matricula.required' => 'La matricula es necesaria',
            'marca.required' => 'La marca es necesaria',
            'modelo.required' => 'El modelo es necesario',
            'unique' => ':attribute ya existe en marcas',
            'matricula.max' => 'La matricula no puede superar los 7 caracteres',
        ];

        $this->validate($request, $reglas, $mensajes);


        $vehiculos = new Vehiculos;

        //Importante el id para la insercion del vehiculo
         $userId = $request->user()->id;
         // fin del campo id


        $vehiculos->matricula = $request->matricula;
        $vehiculos->marca = $request->marca;
        $vehiculos->modelo = $request->modelo;

        if ($request->hasFile('fotografia')){
            //request()->has('image')

            $file = $request->file('fotografia');
            $nombre = time().$file->getClientOriginalName();

            $file->move(public_path().'/storage/img', $nombre);

            // $vehiculos->fotografia = $request->file("fotografia")->getClientOriginalName(). '.' . $request->file("fotografia")->getClientOriginalExtension();
            // $request->file('fotografia')->storeAs('/public/storage/',$vehiculos->fotografia);
        }

        $vehiculos->usuario = $userId;

        $vehiculos->fotografia = $nombre;

        $vehiculos->save();  // Guarda datos en la BD

        //alert()->success('Campo insertado con éxito', $vehiculos->matricula)->persistent('Cerrar');
    }

    public function nuevoVehiculo(){
        $vehiculos = Vehiculos::all();

        return view('vehiculos.nuevo', ['vehiculos'=>$vehiculos]);
    }

    public function edit($id)
    {
        $vehiculos = vehiculos::find($id);

        return view('vehiculos.modificar', compact('vehiculos'));
    }

    public function modificar(Request $request, $id){


        if ( !Auth::check() ) {
           return redirect('/login')->with('info', 'Debe acceder a su cuenta.');
        }

        $userId = $request->user()->id;

        $vehiculos = Vehiculos::findOrFail($id);

        $vehiculos->matricula = $request->matricula;
        $vehiculos->marca = $request->marca;
        $vehiculos->modelo = $request->modelo;

        if ($request->fotografia != null){
            $vehiculos->fotografia = $request->file("fotografia")->getClientOriginalName();
            $request->file('fotografia')->storeAs('/public/img',$vehiculos->fotografia);
        }

        $vehiculos->usuario = $userId;



        $reglas = [
            'matricula' => 'bail|required|max:7',
            'marca' => 'bail|required|max:40',
            'modelo' => 'bail|required|max:40',

        ];

        $mensajes = [
            'matricula.required' => 'La matrícula es obligatoria',
            'matricula.max' => 'La longitud maxima de la matricula es 7',
            'marca.required' => 'El nombre de la marca es obligatorio',
            'marca.max' => 'La marca no puede superar los 40 caracteres',
            'modelo.required' => 'El nombre del modelo es obligatorio',

        ];

         $this->validate($request, $reglas, $mensajes);



            $vehiculos->save();  // Guarda la informaciion en la BD

        // Almacena el archivo en storage/app/public con el nombre $peli->imagen
        //   $request->file('imagen')->storeAs('public',$vehiculo->imagen);

        // volver a la vista actual y, además, con mensaje flash
        return redirect('/vehiculos/index');

    }

public function destroy($id){

        if ( !Auth::check() ) {
           return redirect('/login')
                   ->with('info', 'Para eliminar inicie sesión');
        }

        $vehiculos = Vehiculos::find($id);
        $vehiculos->delete();



        return redirect('/vehiculos/index');

}

}
