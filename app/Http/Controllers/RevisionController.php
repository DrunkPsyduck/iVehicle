<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Revisiones;
use Auth;
use Illuminate\Support\Facades\DB;
use Revisiones as GlobalRevisiones;

class RevisionController extends Controller
{
    //

public function index(){
    // $revisiones = Revisiones::where('idVehiculo', $idVehiculo)->get();

    // return view('revisiones.index')->with('revisiones',$revisiones);
}

public function show($id){

    $revisiones = Revisiones::where('idVehiculo', $id)->get();

    return view('revisiones.show')->with('revisiones', $revisiones);
}

public function store(Request $request ){

        if (!Auth::check()) {
            return redirect('/')
                ->with('info', 'Para dar de alta inicie sesión');
        }

        $reglas = [
            'trabajo' => 'required|max:250',

        ];

        $mensajes = [
            'trabajo.required' => 'Es necesario indicar que trabajo se ha realizado',
        ];

        $this->validate($request, $reglas, $mensajes);


        $revisiones = new Revisiones;

        //Importante el id para la insercion del vehiculo

        // fin del campo id


        $revisiones->TrabajoRealizado = $request->trabajo;
        $revisiones->idVehiculo = $request->idVehiculo;
        $revisiones->FechaRevision = now();


        $revisiones->save();  // Guarda datos en la BD

        //alert()->success('Campo insertado con éxito', $vehiculos->matricula)->persistent('Cerrar');

        return redirect()->to('/revisiones/show/'.$request->idVehiculo);


}

    public function nuevaRevision()
    {

        $revisiones = Revisiones::all();



        return view('revisiones.nueva', compact('revisiones'));
    }

    public function edit($id)
    {
        $revisiones = Revisiones::find($id);

        return view('revisiones.modificar', compact('revisiones'));
    }

    public function modificar(request $request, $id){

        if (!Auth::check()) {
            return redirect('/login')->with('info', 'Debe acceder a su cuenta.');
        }

        $userId = $request->user()->id;

        $revision = Revisiones::findOrFail($id);

       $revision->trabajoRealizado = $request->trabajoRealizado;

       $idVehiculo = $request->idVehiculo;


        $reglas = [
            'trabajoRealizado' => 'bail|required|max:255',


        ];

        $mensajes = [
            'trabajoRealizado.required' => 'Debe indicar que trabajo se ha realizado',
            'trabajoRealizado.max' => 'La longitud maxima es de 255 caracteres',
        ];

        $this->validate($request, $reglas, $mensajes);



        $revision->save();  // Guarda la informaciion en la BD

        // Almacena el archivo en storage/app/public con el nombre $peli->imagen
        //   $request->file('imagen')->storeAs('public',$vehiculo->imagen);

        // volver a la vista actual y, además, con mensaje flash
        return redirect('/revision/show/'.$idVehiculo);
    }


    public function destroy($idRevision)
    {

        if (!Auth::check()) {
            return redirect('/login')
                ->with('info', 'Para eliminar inicie sesión');
        }

        $revision = Revisiones::find($idRevision);

        $idVehiculo = Revisiones::where('idRevision', $idRevision)->pluck('idVehiculo');
        $revision->delete();



        return redirect('/revisiones/show/'.$idVehiculo[0]);
    }

}
