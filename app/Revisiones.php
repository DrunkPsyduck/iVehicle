<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Revisiones extends Model
{
    protected $table = "revisiones";

    protected $primaryKey = 'idRevision';

    protected $fillable = ['trabajoRealizado', 'fechaRevision'];
    protected $guarded = ['idRevision'];
    protected $hidden = ['created_at', 'updated_at'];

    public function vehiculos()
    {
        return $this->belongsTo('App\Vehiculos');
    }
}
