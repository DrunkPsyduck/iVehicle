<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehiculos extends Model
{
    protected $table = "vehiculos";

    protected $increments = 'id';

    protected $fillable = ['matricula', 'marca', 'modelo', 'fotografia','usuario'];

    protected $guarded = ['id'];
    protected $hidden = ['created_at', 'updated_at'];

    public function revisiones()
    {
        return $this->hasMany('App\Revisiones');
    }
}
