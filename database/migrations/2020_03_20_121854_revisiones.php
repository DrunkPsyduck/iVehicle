<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Revisiones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('revisiones', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idRevision');
            $table->string('TrabajoRealizado');
            $table->unsignedInteger('idVehiculo');
            $table->timestamp('FechaRevision');
            $table->timestamps();

        });

        Schema::table('revisiones', function (Blueprint $table) {
            $table->foreign('idVehiculo')->references('id')->on('vehiculos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
