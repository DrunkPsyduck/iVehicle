<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Vehiculos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehiculos', function (Blueprint $table) {

            $table->engine = "InnoDB";
            $table->increments('id');
            $table->string('matricula');
            $table->string('marca');
            $table->string('modelo');
            $table->string('fotografia')->nullable();
            $table->integer('usuario')->unsigned();
            $table->timestamps();

        });

        Schema::table('vehiculos', function (Blueprint $table) {
            $table->foreign('usuario')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
