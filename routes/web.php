<?php

use App\Http\Controllers\RevisionController;
use App\Http\Controllers\VehiculosController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('/home', 'HomeController@index')->name('home');

Route::get('/vehiculos/index', 'VehiculosController@index')->name('vehiculos.index');

Route::get('/vehiculos/nuevo', 'VehiculosController@nuevoVehiculo')->name('vehiculos.nuevo');

Route::get('/vehiculos/modificar/{id}', 'VehiculosController@edit')->name('vehiculos.edit');

Route::put('/vehiculos/modificar/{matricula}', 'VehiculosController@modificar')->name('vehiculos.update');

Route::get('/vehiculos/index/{id}','VehiculosController@destroy')->name('vehiculos.destroy');

//POST imprescindible para el guardado de vehiculos en la base de datos
Route::post('/vehiculos/index','VehiculosController@store');

//Ordenacion
Route::get('/OrdMatriculaAsc', 'VehiculosController@OrdMatriculaAsc');
Route::get('/OrdMatriculaDesc', 'VehiculosController@OrdMatriculaDesc');
Route::get('/OrdMarcaAsc', 'VehiculosController@OrdMarcaAsc');
Route::get('/OrdMarcaDesc', 'VehiculosController@OrdMarcaDesc');
Route::get('/OrdModeloAsc', 'VehiculosController@OrdModeloAsc');
Route::get('/OrdModeloDesc', 'VehiculosController@OrdModeloDesc');

//Fin ordenacion

//Rutas de los metodos de revisiones

Route::get('/revisiones/show/{id}', 'RevisionController@show')->name('revisiones.show');

Route::get('/revisiones/nueva/{id}', 'RevisionController@nuevaRevision')->name('revisiones.nueva');

Route::post('/revisiones/show', 'RevisionController@store')->name('revision.store');

Route::get('/revisiones/modificar/{id}', 'RevisionController@edit')->name('revisiones.edit');

Route::put('/revisiones/modificar/{idRevision}', 'RevisionController@modificar')->name('revisiones.modificar');

Route::get('/revisiones/show{idRevision}', 'RevisionController@destroy')->name('revisiones.destroy');

//Route::get('/revisiones/index/{id}', 'RevisionController@index')->name('vehiculos.index');

// Fin rutas revisiones

Route::get('/CerrarSesion', function () {
    Auth::logout();
    Session::flash('estado', 'Sesión finalizada con éxito !');
    return redirect('/');
});


Auth::routes();
