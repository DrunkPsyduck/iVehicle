{{-- @extends('layouts.app') --}}

@extends('../master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Inicio</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Bienvenido <b>{{ Auth::user()->name }}</b><br/>
                                <a href="{{ url('/vehiculos/index') }}">Vehiculos</a>                </div>
            </div>
        </div>
    </div>
</div>
@endsection
