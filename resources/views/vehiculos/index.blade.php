@extends('../master')

@section('title', 'Vehiculos')

@section('content')

    <div>
        @if(Session::has('exito'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ Session::get('exito') }}
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        @endif
        @if(Session::has('info'))
            <div class="alert alert-info alert-dismissible fade show" role="alert">
                {{ Session::get('info') }}
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        @endif
    </div>

    <div >
        <div >
            <p>{{ $vehiculos }}</p>
            <table  border="1">
                <thead  >
                    <tr>

                        <th>
                            matricula
                            <a href="{{ url('/OrdMatriculaAsc') }}"><i class="blanco fas fa-arrow-up"></i></a>
                            <a href="{{ url('/OrdMatriculaDesc') }}"><i class="blanco fas fa-arrow-down"></i></a>
                        </th>
                        <th>
                            marca
                            <a href="{{ url('/OrdMarcaAsc') }}"><i class="blanco fas fa-arrow-up"></i></a>
                            <a href="{{ url('/OrdMarcaDesc') }}"><i class="blanco fas fa-arrow-down"></i></a>
                        </th>
                        <th>
                            modelo
                            <a href="{{ url('/OrdModeloAsc') }}"><i class="blanco fas fa-arrow-up"></i></a>
                            <a href="{{ url('/OrdModeloDesc') }}"><i class="blanco fas fa-arrow-down"></i></a>
                        </th>
                        <th>fotografia</th>
                        <th>Acciones</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach ($vehiculos as $v)
                    <tr>
                        <td>{{ $v->matricula }}</td>
                        <td>{{ $v->marca }}</td>
                        <td>{{ $v->modelo }}</td>
                        <td>
                            @if($v->fotografia == null)
                                <img src=" {{ url('/img/car.svg') }}" height="auto" width="40%" height="auto" alt="vehiculo" title="vehiculo"/>
                            @else
                                <img src="/storage/img/{{$v->fotografia}}" width="20%" height="auto" alt="vehiculo" title="vehiculo"/>
                                {{-- {{$v->fotografia}} --}}
                            @endif
                        </td>
                        <td>
                            <a href="{{ route('revisiones.show',$v->id) }}"> <img src=" {{ url('/img/tool.svg') }}" height="auto" width="20%" alt="revisiones" title="Revisiones"/>  </a>

                            <a href="{{ route('revisiones.nueva', $v->id) }}"><img src=" {{ url('/img/plus.svg') }}" height="auto" width="20%" alt="Añadir nueva revision" title="Añadir nueva revision"/></a>

                            <a href="{{ route('vehiculos.edit',$v->id) }}"> <img src=" {{ url('/img/settings.svg') }}" height="auto" width="20%" alt="modificar" title="Modificar"/> </a>

                            <a href="{{ route('vehiculos.destroy',['id' => $v->id] ) }}"> <img src=" {{ url('/img/trash.svg') }}" height="auto" width="20%" alt="Eliminar" title="Eliminar"/></a>
                        </td> <!-- Link a la imagen: https://feathericons.com/?query=trash -->
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="row">

            </div>
            <a href="{{ route('vehiculos.nuevo') }}"><img src=" {{ url('/img/plus.svg') }}" height="auto" width="2%" alt="Añadir nuevo vehiculo" title="Añadir nuevo vehiculo"/></a>
        </div>

    </div>

@endsection

