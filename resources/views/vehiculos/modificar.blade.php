@extends('../master')

@section('title', 'Modificar vehiculo')

@section('content')
    <hr>
    <div id="p" class="row">
        <h4>Modificar {{ $vehiculos->id }}</h4>
    </div>

    <div class="row shadow p-4 mb-4 bg-white">
        <!-- Matricula -->
        <div id="izq">
            <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data"
                            type="hidden" action="{{ route('vehiculos.update',$vehiculos->id) }}">
                            {{ method_field('PUT') }}
                            {{ csrf_field() }}




                <input id="id" type="hidden" name="id" value="{{ $vehiculos->matricula }}"/>

                <div class="form-group">
                    <label for="matricula">matricula:</label>
                    <input id="matricula" type="text" class="form-control col-9" name="matricula"
                           value="{{ $vehiculos->matricula }}" autofocus/>
                    @if ($errors->has('matricula'))
                        <span class="help-block">
                            <strong>{{ $errors->first('matricula') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="row">
                    <!-- Marca -->
                    <div class="form-group">
                        <label for="marca">marca:</label>
                        <input id="marca" type="text" class="form-control col-10" name="marca"
                                value="{{ $vehiculos->marca }}" autofocus>
                        @if ($errors->has('marca'))
                            <span class="help-block">
                                <strong>{{ $errors->first('marca') }}</strong>
                            </span>
                        @endif
                    </div>
                    <!-- Modelo -->
                    <div class="form-group">
                        <label for="modelo">modelo:</label>
                        <input id="modelo" type="text" class="form-control col-6" name="modelo"
                                value="{{ $vehiculos->modelo }}" autofocus>
                        @if ($errors->has('modelo'))
                            <span class="help-block">
                                <strong>{{ $errors->first('modelo') }}</strong>
                            </span>
                        @endif
                    </div>

                    <!-- Fotografia -->
                   <div class="form-group">
                        <label for="fotografia">fotografia:</label>
                        <input id="fotografia" type="file" class="form-control col-6" name="fotografia"
                                value="{{ $vehiculos->fotografia }}" autofocus>
                    </div>

                </div>

                <div class="form-group">
                    <div class="col-lg-offset-4 col-8">
                        <button type="submit" class="btn btn-primary">Actualizar</button>

                    </div>
                </div>
            </form>
        </div>


    </div>

    <div class="row">
         @if(Session::has('exito'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ Session::get('exito') }}
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        @endif
        @if(Session::has('info'))
            <div class="alert alert-info alert-dismissible fade show" role="alert">
                {{ Session::get('info') }}
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        @endif
    </div>

@endsection
