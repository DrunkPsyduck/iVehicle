@extends('../master')

@section('title', 'Nuevo vehiculo')

@section('content')

 <div class="row">
        @if(Session::has('exito'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ Session::get('exito') }}
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        @endif
        @if(Session::has('info'))
            <div class="alert alert-info alert-dismissible fade show" role="alert">
                {{ Session::get('info') }}
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        @endif
    </div>

    <div class="row shadow p-4 mb-4 bg-white">

        <form class="form-horizontal" role="form" method="post"
                enctype="multipart/form-data" action="{{ url('/vehiculos/index') }}">
            {{ csrf_field() }}


               <div class="form-group">
                <label for="matricula"> Matricula:</label>
                    <input id="nombre" type="text" class="form-control col-12" name="matricula"
                           value="" autofocus/>
                    @if ($errors->has('matricula'))
                        <span class="help-block">
                            <strong>{{ $errors->first('matricula') }}</strong>
                        </span>
                    @endif
            </div>

               <div class="form-group">
                <label for="marca">Marca:</label>
                    <input id="marca" type="text" class="form-control col-12" name="marca"
                           value="" autofocus/>
                    @if ($errors->has('marca'))
                        <span class="help-block">
                            <strong>{{ $errors->first('marca') }}</strong>
                        </span>
                    @endif
            </div>

               <div class="form-group">
                <label for="modelo">Modelo:</label>
                    <input id="modelo" type="text" class="form-control col-12" name="modelo"
                           value="" autofocus/>
                    @if ($errors->has('modelo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('modelo') }}</strong>
                        </span>
                    @endif
            </div>

            <div class="form-group">
                <label for="fotografia">Fotografia:</label>
                    <input id="fotografia" type="file" multiple data-preview-file-type="any" data-upload-url="#"  class="form-control col-12" name="fotografia"
                           value="" autofocus/>
                    @if ($errors->has('fotografia'))
                        <span class="help-block">
                            <strong>{{ $errors->first('fotografia') }}</strong>
                        </span>
                    @endif
            </div>

            <div class="form-group">
                <div class="col-lg-offset-4 col-lg-8">
                    <button type="submit" class="btn btn-primary">Insertar</button>
                </div>
            </div>
        </form>
 <a href="{{ route('vehiculos.index') }}">Volver a vehiculos</a>
    </div>

@endsection
