@extends('../master')

@section('title', 'Mostar revisiones')

@section('content')

    <div id="p" class="row">

    </div>

    @if (count($revisiones) > 0 )
    <div class="shadow p-4 mb-4 bg-white">
        <table class="table table-striped table-hover" border="1">
            <thead class="thead-dark">
                <tr>
                    <th>Codigo revisión</th>

                    <th>Trabajo Realizado</th>
                    <th>fecha revision</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($revisiones as $rev)


                    <tr>
                    <td>{{ $rev->idRevision}}</td>
                    <td>{{ $rev->TrabajoRealizado }}</td>
                    <td>{{ $rev->FechaRevision}}</td>
                    <td>
                        <a href="{{ route('revisiones.destroy',['idRevision' => $rev->idRevision]) }}"> <img src=" {{ url('/img/trash.svg') }}" height="auto" width="20%" alt="Eliminar" title="Eliminar"/></a>
                        <a href="{{route('revisiones.edit', $rev->idRevision) }}"> <img src=" {{ url('/img/tool.svg') }}" height="auto" width="20%" alt="Modificar" title="Modificar"/></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <a class="btn btn-outline-info" href="{{ route('revisiones.nueva', $rev->idVehiculo) }}">Nueva revision</a></td>

        @else
            <p> No existen revisiones para este vehiculo </p>
        @endif

        <a class="btn btn-outline-info" href="{{ route('vehiculos.index') }}">volver a ver sus vehiculos</a></td>

    </div>


    <div class="row">
         @if(Session::has('exito'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ Session::get('exito') }}
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        @endif
        @if(Session::has('info'))
            <div class="alert alert-info alert-dismissible fade show" role="alert">
                {{ Session::get('info') }}
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        @endif
    </div>

@endsection
