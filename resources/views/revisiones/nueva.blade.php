@extends('../master')

@section('title', 'Anadir revision')

@section('content')

<script>
    function getId(){
    var url_string = window.location.href
    var url = url_string.split('/');
    var id = url.pop();
    console.log(id);

    document.getElementById('idVehiculo').value = id;

    console.log(document.getElementById('idVehiculo'))

}
 window.onload = getId;
 console.log(document.getElementById('idVehiculo'))
    </script>

 <div class="row">
        @if(Session::has('exito'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ Session::get('exito') }}
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        @endif
        @if(Session::has('info'))
            <div class="alert alert-info alert-dismissible fade show" role="alert">
                {{ Session::get('info') }}
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        @endif
    </div>

    <div class="row shadow p-4 mb-4 bg-white">
        <form class="form-horizontal" role="form" method="post"
    enctype="multipart/form-data" action="{{ route('revision.store', ['idVehiculo' => $revisiones])}}">
            {{ csrf_field() }}


            <div class="form-group">
                <label for="trabajo"> Trabajo realizado:</label>
                    <input id="nombre" type="text" class="form-control col-12" name="trabajo"
                           value="" autofocus/>
                    @if ($errors->has('trabajo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('trabajo') }}</strong>
                        </span>
                    @endif
            </div>

            <div class="form-group">

                    <input id="idVehiculo" type="hidden" class="form-control col-12" name="idVehiculo"
                        value='' autofocus/>

            </div>




            <div class="form-group">
                <div class="col-lg-offset-4 col-lg-8">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </form>
 <a href="{{ url('/vehiculos/index') }}">Volver a vehiculos</a>
    </div>

@endsection
