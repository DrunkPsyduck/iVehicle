@extends('../master')

@section('title', 'Modificar vehiculo')

@section('content')

<script>
    function getId(){
    var url_string = window.location.href
    var url = url_string.split('/');
    var id = url.pop();
    console.log(id);

    document.getElementById('idVehiculo').value = id;

    console.log(document.getElementById('idVehiculo'))

}
     window.onload = getId;
     console.log(document.getElementById('idVehiculo'))
    </script>

    <hr>
    <div id="p" class="row">
        <h4>Modificar {{ $revisiones->id }}</h4>
    </div>

    <div class="row shadow p-4 mb-4 bg-white">
        <!-- Matricula -->
        <div id="izq">
            <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data"
                            type="hidden" action="{{ route('revisiones.modificar',$revisiones->idRevision) }}">
                            {{ method_field('PUT') }}
                            {{ csrf_field() }}

                <div class="form-group">
                    <label for="trabajoRealizado">Trabajo realizado:</label>
                    <input id="trabajoRealizado" type="text" class="form-control col-9" name="trabajoRealizado"
                           value="{{ $revisiones->trabajoRealizado }}" autofocus/>
                    @if ($errors->has('trabajoRealizado'))
                        <span class="help-block">
                            <strong>{{ $errors->first('trabajoRealizado') }}</strong>
                        </span>
                    @endif
                </div>

                   <div class="form-group">

                    <input id="idVehiculo" type="hidden" class="form-control col-12" name="idVehiculo"
                        value='' autofocus/>

            </div>

                <div class="form-group">
                    <div class="col-lg-offset-4 col-8">
                        <button type="submit" class="btn btn-primary">Actualizar</button>

                    </div>
                </div>
            </form>
        </div>


    </div>

    <div class="row">
         @if(Session::has('exito'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ Session::get('exito') }}
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        @endif
        @if(Session::has('info'))
            <div class="alert alert-info alert-dismissible fade show" role="alert">
                {{ Session::get('info') }}
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        @endif
    </div>

@endsection
