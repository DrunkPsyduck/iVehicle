<!DOCTYPE html>
<html lang="es">
    <head>
        <title> @yield('title')</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
        <link rel='stylesheet' href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.min.css">
        <link rel=”stylesheet” href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">

        <link rel='stylesheet' type='text/css' href="{{ asset('css/estilos.css') }}" />

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.min.js"></script>

        <link href="{{ asset('css/app.css') }}" rel="stylesheet">


<!-- Scripts -->
        <script src="{{ asset('js/app.js') }}"></script>

        <style>
            .maxWith{
                width: 100%;
            }
            .bgcolor{
                background-color: #bbd9ee;
            }

            #cuerpo{
                padding: 3%;
            }
            .navbar-default{
                width: 100%;
                font-weight: bold ;
                background-color: #ffab35;
            }
            .container {
                padding: 0px !important;
                width: : 100%
            }

            nav.navbar {
                width: 100%;
                margin: 0% !important;
            }

        </style>

    </head>

    <body class="bgcolor">
        <div id="contenedor" class="container-fluid m-0">


            @include('navbar')

            <div id="cuerpo" class="container-fluid m-0">
                @yield('content')
            </div>


        </div>

        <script src="{{ asset('js/ajax.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>


        <footer id="pie" class="page-footer maxWidth" >
            @include('pie')
        </footer>
    </body>
</html>
