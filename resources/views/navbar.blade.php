<nav class="navbar navbar-expand-sm navbar-default " role="navigation">
    <div id="menunav" class="container-fluid">
        <ul class="navbar-nav">
          <li class="nav-item {{ request()->is('Vehiculos') ? 'active activa' : '' }}">
              <a class="nav-link" href="{{ route('vehiculos.index') }}">Vehiculos</a></li>
          {{-- @auth
          <li class="nav-item {{ request()->is('marcas/nueva') ? 'active activa' : '' }}">
              <a class="nav-link" href="{{ route('marcas.nueva') }}">
                  <i class="fas fa-plus"></i> Nueva Marca</a></li>

          <li class="nav-item {{ request()->is('modelos/nueva') ? 'active activa' : '' }}">
              <a class="nav-link" href="{{ route('modelo.nuevo') }}">
                  <i class="fas fa-plus"></i> Nuevo Modelo</a></li>
          @endauth --}}
        </ul>
        <ul class="navbar-nav">
          @auth

          <li class="nav-item"><a class="nav-link" href="/CerrarSesion"><i class="fas fa-sign-out-alt"></i> Cerrar Sesión</a></li>
          <li class="nav-link" style="color: #ffe817;">{{ Auth::user()->name }}</li>
          @else
          <li class="nav-item {{ request()->is('login') ? 'active activa' : '' }}">
              <a class="nav-link" href="{{ route('login') }}"><i class="fas fa-sign-in-alt"></i> Iniciar sesión</a></li>
              <li class="nav-item {{ request()->is('register') ? 'active activa' : '' }}">
                <a class="nav-link" href="{{ route('register') }}"><i class="fas fa-sign-in-alt"></i> Registrarse</a></li>
          <li class="nav-item {{ request()->is('/') ? 'active activa' : '' }}">
              <a class="nav-link" href="/home"><i class="fas fa-home"></i> Inicio</a></li>
          @endauth
        </ul>
    </div>
</nav>
