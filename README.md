# iVehicle
![GitHub code size in bytes](https://img.shields.io/github/languages/code-size/DrunkPsyduck/iVehicle) 
![GitHub commit activity](https://img.shields.io/github/commit-activity/m/DrunkPsyduck/iVehicle)
![GitHub commit activity](https://img.shields.io/github/commit-activity/w/DrunkPsyduck/iVehicle) 
![GitHub contributors](https://img.shields.io/github/contributors/DrunkPsyduck/iVehicle)
![GitHub last commit](https://img.shields.io/github/last-commit/DrunkPsyduck/iVehicle)
![GitHub language count](https://img.shields.io/github/languages/count/DrunkPsyduck/iVehicle)
![GitHub](https://img.shields.io/github/license/DrunkPsyduck/iVehicle?style=for-the-badge)
[]

iVehicle es una aplicacion que permite a los usuarios llevar el control sobre el mantenimiento del vehiculo desde sus dispositivos, sin la necesidad de ver las hojas de mantenimiento del vehiculo.

*Este repositorio se corresponde con el módulo de Proyecto de Fin de Grado del Grado Superior de Desarrollo de Aplicaciones Web*

## Funcionamiento
### Pre-Requisitos
Para poder usar iVehicle, como es obvio, se debe disponer de una navegador web y una conexión a internet.

### Instalación
Esta aplicación web estara alojada en un dominio, el cual aún no se ha decidido.

## Desarrollado con:
  El desarrollo de esta aplicación sera con:
  - HTML5
  - CSS3
  - Bootstrap
  - Javascript ES6
  - React JS
  - AJAX
  - Laravel
  
  **NOTA: El uso de estas tecnologias puede variar**
  
  ## Documentacion/wiki 📖

Puedes encontrar mucha más información sobre el iVehicle y sobre su uso en la [Wiki](https://github.com/DrunkPsyduck/iVehicle/wiki)
  
## Autores ✒️

* **Mario Canales** - - [DrunkPsyduck](https://github.com/DrunkPsyduck)

[GitHub Page](https://drunkpsyduck.github.io/iVehicle/)
